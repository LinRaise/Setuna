﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Setuna {
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();
        }

        void UpdateLanguage() {
            menExit.Text = "Exit";
        }

        protected override void WndProc(ref Message m) {
            const int WM_HOTKEY = 0x0312;
            if (m.Msg == WM_HOTKEY) {
                switch (m.WParam.ToInt32()) {
                    case 1000:
                        OnHotkeyClick();
                        break;
                }
            }
            base.WndProc(ref m);
        }

        void OnHotkeyClick() {
            if (_bCapturing) return;

            _bCapturing = true;

            Image screenImg = ScreenCapture.Capture();

            Capturer capturer = new Capturer(screenImg);
            capturer.FormClosed += delegate {
                _bCapturing = false;

                if (capturer.CaptureImage != null) {
                    Shower shower = new Shower(capturer.Position.X, capturer.Position.Y, capturer.CaptureImage);
                    shower.Show();
                }
            };
            capturer.ShowDialog();
        }

        void MainForm_Load(object sender, EventArgs e) {
            Close();

            UpdateLanguage();

            if (!Hotkey.RegisterHotKey(Handle, 1000, Hotkey.KeyModifiers.Ctrl, Keys.D1)) {
                MessageBox.Show("Can\'t register hot key for Setuna.");
                Application.Exit();
            }
        }

        void MainForm_FormClosing(object sender, FormClosingEventArgs e) {
            e.Cancel = true;


            Hide();
            ShowInTaskbar = false;
            _notifyIcon.Visible = true;
        }

        void MainForm_Leave(object sender, EventArgs e) {
            Hotkey.UnregisterHotKey(Handle, 1000);
        }

        void menExit_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        bool _bCapturing = false;
    }
}

