﻿using System.Drawing;
using System.Windows.Forms;

namespace Setuna {
    public partial class Capturer : Form {
        public Capturer(Image screen) {
            InitializeComponent();

            _screen = screen;
            _screenGrap = Graphics.FromImage(_screen);

            _screenBackup = new Bitmap(_screen);
        }

        public Image CaptureImage { get { return _out; } }
        public Point Position {
            get {
                var p = PointToScreen(_minPoint);
                return new Point(p.X - 3, p.Y - 3);
            }
        }

        void Capturer_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Escape) {
                Close();
            }
        }

        void Capturer_Load(object sender, System.EventArgs e) {
            BackgroundImage = _screen;

            int minx = int.MaxValue;
            int miny = int.MaxValue;

            foreach (var screen in Screen.AllScreens) {
                if (minx >= screen.Bounds.X) minx = screen.Bounds.X;
                if (miny >= screen.Bounds.Y) miny = screen.Bounds.Y;
            }

            Location = new Point(minx, miny);

            Width = _screen.Width;
            Height = _screen.Height;
        }

        void Capturer_MouseMove(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                if (!_bIsDown) {
                    _downPoint = e.Location;
                    _bIsDown = true;
                }

                Point upPoint = e.Location;

                _minx = upPoint.X;
                _miny = upPoint.Y;
                _maxx = upPoint.X;
                _maxy = upPoint.Y;

                if (_downPoint.X < _minx) _minx = _downPoint.X;
                if (_downPoint.Y < _miny) _miny = _downPoint.Y;

                if (_downPoint.X > _maxx) _maxx = _downPoint.X;
                if (_downPoint.Y > _maxy) _maxy = _downPoint.Y;

                _minPoint = new Point(_minx, _miny);

                _screenGrap.DrawImage(_screenBackup, new Point(0, 0));
                _screenGrap.DrawRectangle(new Pen(Color.Red), _minx, _miny, _maxx - _minx, _maxy - _miny);

                BackgroundImage = _screen;

                Refresh();
            }
            else {
                if (_bIsDown) {
                    _bIsDown = false;

                    int w = _maxx - _minx + 3;
                    int h = _maxy - _miny + 3;

                    _out = new Bitmap(w, h);
                    Graphics g = Graphics.FromImage(_out);
                    g.DrawImage(_screenBackup, 2, 2, new Rectangle(_minx, _miny, _maxx - _minx, _maxy - _miny), GraphicsUnit.Pixel);

                    Pen whitePen = new Pen(Color.White);
                    g.DrawLine(whitePen, 0, 0, 0, h - 2);
                    g.DrawLine(whitePen, 1, 0, 0, h - 2);

                    g.DrawLine(whitePen, 0, 0, w - 1, 0);
                    g.DrawLine(whitePen, 0, 1, w - 1, 1);

                    Pen grayPen = new Pen(Color.Gray);
                    g.DrawLine(grayPen, 0, h - 1, w - 1, h - 1);
                    g.DrawLine(grayPen, w - 1, 2, w - 1, h - 1);

                    Close();
                }
            }
        }

        bool _bIsDown = false;
        Point _downPoint;
        Point _minPoint;

        int _minx;
        int _miny;
        int _maxx;
        int _maxy;

        Image _screen;
        Image _screenBackup;
        Image _out;

        Graphics _screenGrap;
    }
}
