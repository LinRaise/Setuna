﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;


namespace Setuna {
    public partial class Shower : Form {
        public Shower(int x, int y, Image picture) {
            InitializeComponent();

            _picture = picture;
            _location = new Point(x, y);
        }

        void Shower_Load(object sender, EventArgs e) {
            BackgroundImage = _picture;

            Location = _location;

            Width = _picture.Width;
            Height = _picture.Height;

            Clipboard.SetImage(GetRealImage());
        }

        void Shower_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Escape && _bIsEnter) {
                ctxMenu.Hide();
                Close();
            }
        }

        void Shower_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Right) {
                _bShowMenu = true;
                ctxMenu.Show(PointToScreen(e.Location));
            }

            if (e.Button == MouseButtons.Left) {
                if (_bShowMenu) {
                    _bShowMenu = false;
                    ctxMenu.Hide();
                }
                _bIsDown = true;
                _downPoint = e.Location;
            }
        }

        void Shower_MouseUp(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                _bIsDown = false;
            }
        }

        void Shower_MouseMove(object sender, MouseEventArgs e) {
            if (_bIsDown) {
                _location.X += e.Location.X - _downPoint.X;
                _location.Y += e.Location.Y - _downPoint.Y;

                Location = _location;
            }
        }

        void Shower_MouseEnter(object sender, EventArgs e) {
            _bIsEnter = true;
        }

        void Shower_MouseLeave(object sender, EventArgs e) {
            _bIsEnter = false;
        }

        Image _picture;
        Point _location;

        Point _downPoint;

        bool _bIsDown = false;
        bool _bIsEnter = false;

        bool _bShowMenu = false;

        void MenuClose_Click(object sender, EventArgs e) {
            Close();
        }

        void MenuCopy_Click(object sender, EventArgs e) {
            Clipboard.SetImage(GetRealImage());
        }

        void MenuSave_Click(object sender, EventArgs e) {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.DefaultExt = "*.png";
            dlg.Filter = "PNG file|*.png";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == DialogResult.OK) {
                GetRealImage().Save(dlg.FileName, ImageFormat.Png);
            }
        }

        void MenuSettings_Click(object sender, EventArgs e) {

        }

        Image GetRealImage() {
            Image tmp = new Bitmap(BackgroundImage.Width - 3, BackgroundImage.Height - 3);
            Graphics g = Graphics.FromImage(tmp);
            g.DrawImage(BackgroundImage, 0, 0, new Rectangle(2, 2, BackgroundImage.Width - 3, BackgroundImage.Height - 3), GraphicsUnit.Pixel);
            return tmp;
        }
    }
}
