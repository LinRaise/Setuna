﻿using System.Drawing;
using System.Windows.Forms;

namespace Setuna {
    class ScreenCapture {
        public static Image Capture() {
            Rectangle screenBound = new Rectangle();

            int x = int.MaxValue;
            int y = int.MaxValue;

            foreach (var screen in Screen.AllScreens) {
                screenBound = Rectangle.Union(screenBound, screen.Bounds);

                if (x >= screen.Bounds.X) x = screen.Bounds.X;
                if (y >= screen.Bounds.Y) y = screen.Bounds.Y;
            }

            int height = screenBound.Height;
            int width = screenBound.Width;

            if (_screenImg == null || width > _screenImg.Width || height > _screenImg.Height) {
                _screenImg = new Bitmap(width, height);
                _graphics = Graphics.FromImage(_screenImg);
            }

            _graphics.CopyFromScreen(x, y, 0, 0, new Size(width, height));

            return _screenImg;
        }

        static Image _screenImg;
        static Graphics _graphics;
    }
}
