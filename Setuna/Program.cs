﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace Setuna {
    static class Program {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main() {
            Mutex mutex = new Mutex(true, Application.ProductName, out bool ret);

            if (ret) {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());

                mutex.ReleaseMutex();
            }
            else {
                
                MessageBox.Show("Setuna already runing.", Application.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);  
                Application.Exit();
            }
        }
    }
}
